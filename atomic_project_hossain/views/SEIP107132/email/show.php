<?php

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Email\EmailSub;
 
$email = new EmailSub();
$mails =$email->show($_GET['id']);

//var_dump($mails);
 


?>


<!DOCTYPE html>
<!--
-->
<html>
    <head>
        <title>Organization</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
               
               
   
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon icon-birthday-cake"></span> 
            Email
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="create.php">Add</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
                
     

                    
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Email Subscriber List</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>


            
        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">
                
            
           
            <table class="table table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Trash</th>
                <th>Email</th>
                
            </tr>
            </thead>
            
            <tbody>
            <tr >
                <td><?php echo $mails['id'];?></td>
                <td><?php echo $mails['name'];?></td>
                <td><?php echo $mails['email'];?></td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            </tbody>   
            </table>
            </div> 
            <div class="col-md-2">
            </div>   
            </div>     
       

 
 
      
            <div class="alert alert-success" role="alert" center-block>
                <p class="text-center">Design & Develop by
                <a href="https://www.facebook.com/ahmed.a.hossain">@Hossain</a></p>
            </div>           
        
           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
