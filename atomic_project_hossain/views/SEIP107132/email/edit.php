<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\email\EmailSub;

$emailobj = new EmailSub();
$mails =$emailobj->show($_GET['id']);


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Email</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
        
   
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-organization"></span> 
            Email
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="create.php"> Add</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
                
     

                    
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Edit Email</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>


            
            <div class="container">
                <div class="jumbotron">

                    <form role="form" action="update.php" method="post">
                          <div class="form-group">
                              
                         <input  type="hidden"  name="id"  value="<?php echo $mails['id'];?>" >
                             
                        <label for="title" >Edit Name</label>
                        <input autofocus="autofocus"
                              id="title"
                              placeholder="Enter the email" 
                              type="text" 
                              name="name"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $mails['name'];?>"
                        >
                        
                        <label for="title" >Edit Email</label>
                        <input autofocus="autofocus"
                              id="title"
                              placeholder="Enter the email" 
                              type="text" 
                              name="email"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $mails['email'];?>"
                        >
                        
                        
                        
                        </div>
                        <button  type="submit" class="btn btn-primary">Update</button>
                        <button  type="submit" class="btn btn-success">Reset</button>
   

                        </form>  
                </div>
            </div>

      
        
        <div class="alert alert-success" role="alert" center-block>
        <p class="text-center">Design & Develop by
        <a href="https://www.facebook.com/ahmed.a.hossain">@Hossain</a></p>
        </div>           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
