<!DOCTYPE html>
<html>
    <head>
        <title>Profile</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-gift"></span> 
                        Profile
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="create.php">Add</a></li>
                        <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">Profile Picture</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>    

        <div class="container">
            <a href="create.php"> <button type="button" class="btn btn-success pull-right" >Add new picture</button></a>
        </div>

        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">

                <table id="list" class="table table-responsive table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>01</td>
                            <td>Hossain</td>
                            <td>gsdfgf.png</td>
                        </tr>

                    </tbody>
                </table>



            </div> 
            <div class="col-md-2">
            </div>   
        </div>     

        <!-- pager -->

        <div class="container">
            <ul class="pager">
                <li><a href="#">Previous</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </div>

        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
