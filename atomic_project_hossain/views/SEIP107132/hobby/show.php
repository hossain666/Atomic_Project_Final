<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\hobby\MyHobby;


$hobbyobj = new MyHobby();
$hobbies = $hobbyobj->show($_GET['id']);

//var_dump($_GET['id']); die();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hobby</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
  
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-education"></span> 
            Hobby
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="index.php">List View</a></li>
                     </ul>
                </div>
            </div>
           </nav>
      
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Hobby Info List</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>
  
            <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">
                                     
            <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Hobby</th>
                <th>Action</th>
                
                
            </tr>
            </thead>
            
            <tbody>
            <tr >
                <td><?php echo $hobbies ['id'];?></td>
                <td><?php echo $hobbies ['name'];?></td>
                <td><?php echo $hobbies ['hobby'];?></td>
                <td>
                <a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></a>
                <a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></a>
                <a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></a>
                <button type="button" class="btn btn-danger">Trash</button>   
                <button type="button" class="btn btn-success">Email</button> 
                </td>  
            </tr>
            
            
            </tbody>   
            </table>
            </div> 
            <div class="col-md-2">
            </div>   
            </div>     
       

             <!-- Pager -->
                <ul class="pager">
                  <li><a href="#">Previous</a></li>
                  
                  <!--Pagination -->
                    <ul class="pagination">
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                 
                    <li><a href="#">Next</a></li>
                </ul>
 
            <!--footer --> 
             <div class="container">
                <div class="panel panel-default">
                    <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                    <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                    </div>
                </div>
             </div>

           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
