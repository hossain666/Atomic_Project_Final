<?php

    include_once '../../../vendor/autoload.php';
    use App\Bitm\SEIP107132\hobby\MyHobby;
    
    
    $hobbyObj = new MyHobby();
    $hobby = $hobbyObj->show($_GET['id']);
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Hobby</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
  
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-education"></span> 
                        Hobby
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="index.php">List View</a></li>
                     </ul>
                </div>
            </div>
        </nav>
          
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Edit Your Hobby</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>
  
        <div class="container">
                <div class="jumbotron">

                    <form role="form" action="update.php" method="post"> 
                      <input type="hidden" name="id" value="<?php echo $hobby['id']?>">
                         <div class="form-group">                        
                        <label for="title" >Name</label>
                        <input type="text" 
                               id="name" 
                               name="name" 
                               placeholder="Enter the name" 
                               class="form-control"
                               value="<?php echo $hobby ['name'];?>"
                               
                               >
                        <label for="title" >Hobby</label>
                        <input autofocus="autofocus"
                              id="hobby"
                              placeholder="Enter the hobby" 
                              type="text" 
                              name="hobby"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $hobby ['hobby'];?>"
                        >
                        </div>
                        <button  type="submit" class="btn btn-primary">Update</button>
   

                        </form>  
                </div>
            </div>

                        
                
        
            
      
        
        <div class="alert alert-success" role="alert" center-block>
        <p class="text-center">Design & Develop by
        <a href="https://www.facebook.com/ahmed.a.hossain">@Hossain</a></p>
        </div>           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
