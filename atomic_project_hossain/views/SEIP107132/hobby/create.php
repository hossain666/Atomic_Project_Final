<!DOCTYPE html>
<!--
-->
<html>
    <head>
        <title>Hobby</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
               
               
   
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-education"></span> 
            Hobby
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="index.php">List View</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
                
     

                    
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Check Your Hobby</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>
        
            
            <div class="row">
            <div class="col-md-3">
            </div>    
            <div class="col-md-6">
              
              <div class="jumbotron">
                <div class="container">
                    <form role="form" action="store.php" method="POST">
                        <div class="form-group">
                            <label>Name: </label>
                            <input type="text" name="name" class="form-control"/>
                        </div>
                      <div class="checkbox">
                        <label><input type="checkbox" name="hobbies[]" value="Gaming">Gaming</label>
                      </div>
                      <div class="checkbox">
                        <label><input type="checkbox" name="hobbies[]" value="Playing">Playing</label>
                      </div>
                      
                      <div class="checkbox">
                        <label><input type="checkbox" name="hobbies[]" value="Traveling">Traveling</label>
                      </div>
                      <div class="checkbox">
                        <label><input type="checkbox" name="hobbies[]" value="Swiming">Swiming</label>
                      </div>
                        <button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-plus-sign"></span>Add</button>
                    </form>
                  
                  </div>
              </div>


            
            </div> 
            <div class="col-md-3">
            </div>   
            </div>     
       

             
            <!--footer --> 
             <div class="container">
                <div class="panel panel-default">
                    <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                    <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                    </div>
                </div>
             </div>

           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
