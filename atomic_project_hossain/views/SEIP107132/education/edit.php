<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Utility\Message;
use App\Bitm\SEIP107132\education\EducationLevel;

$eduobj = new EducationLevel();
$edu = $eduobj->show($_GET['id']);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Education</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-education"></span> 
            Education 
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="#"> View</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
       
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center"> New Education Level</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>

        <div class="container">
                <div class="jumbotron">

                    <form role="form" action="update.php" method="POST">
                        <input type="hidden" name="id" value="<?php echo $edu['id']?>" >
                        <div class="form-group">
                        <label for="title">Name</label>
                        <input autofocus="autofocus"
                              id="name"
                              placeholder="Enter the education level" 
                              type="text" 
                              name="name"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $edu['name']?>"
                        >
                        <label for="title">Education Level</label>
                        <input autofocus="autofocus"
                              id="level"                             
                              type="radio" 
                              name="level"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $edu['level']?>"
                        >
                        
                        
                        <input type="checkbox" 
                               name="hobbies[]" value="Photography" <?= (strstr($hobby->hobbies, 'Photography'))?'checked="checked"':'' ?>>Photography
                        
                        </div>
                        <button  type="submit" class="btn btn-primary">Update</button> 
                        </form>  
                </div>
            </div>

      <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>         

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
