<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Utility\Message;
use App\Bitm\SEIP107132\education\EducationLevel;

$eduobj = new EducationLevel();
$edu = $eduobj->trashed();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Education</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
       
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-education"></span> 
                        Education
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>                       
                        <li><a href="create.php">Add</a></li> 
                    </ul>
                </div>
        </nav>
       
        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">Level of Education</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>
        
        <div class="container">
        <?php
        echo Message::message();
        ?>
        </div>

        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Education Level</th>
                            <th>Action</th>               
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $serial= 1;
                        foreach($edu as $edusingle){
                        ?>
                        <tr>
                            <td><?php echo $serial['id'];?></td>
                            <td><?php echo $edusingle['name'];?></td>
                            <td><?php echo $edusingle['level'];?></td>               
                            <td><a href="show.php?id=<?php echo $edusingle['id'];?>"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></a></td>
                            <td>
                                <form action="delete.php" method="POST">
                                       <input type="hidden" name="id" value="<?php echo $edusingle['id'];?>">
                                       <button type="submit" class="btn btn-danger deleteBtn"><span class="glyphicon glyphicon-remove"></span> Delete</button>
                                </form>
                            </td>
                            <td><a href="recover.php?id=<?php echo $edusingle['id'];?>"class="btn btn-info"> Recover</a> </td>  
                            <td><button type="button" class="btn btn-success">Email</button> </td>  
                        </tr>
                        <?php 
                        $serial++;
                        }
                        ?>
                    </tbody>   
                </table>
            </div> 
            <div class="col-md-2">
            </div>   
        </div>     


        <!-- Pager -->
        <ul class="pager">
            <li><a href="#">Previous</a></li>

            <!--Pagination -->
            <ul class="pagination">
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>

            <li><a href="#">Next</a></li>
        </ul>

        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>






      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../../resource/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script>
            $('.alert').fadeOut(4000);
            
            $('.deleteBtn').bind('click', function(e){
                if(confirm("Do you want to delete this?") == false){
                    e.preventDefault();
                }
            });
        </script>
    </body>
</html>
