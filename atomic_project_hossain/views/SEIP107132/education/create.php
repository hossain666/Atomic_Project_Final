<!DOCTYPE html>
<html>
    <head>
        <title>Education</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-education"></span> 
                        Education 
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="#"> View</a></li>
                        <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center"> New Education Level</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>

        <div class="container">
            <div class="jumbotron">

                <div class="container">
                    <!-- Example row of columns -->
                    <div class="row">
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-8">
                            <form role="form" action="store.php" method="post">                                    
                                <div class="form-group">
                                    <label for="title">Name</label>
                                    <input autofocus="autofocus"
                                           id="name"
                                           placeholder="Enter your name" 
                                           type="text" 
                                           name="name"
                                           required="required"
                                           size="30"
                                           class="form-control"
                                           >
                                </div>

                                <div class="form-group">
                                    <div><label><input type="radio" name="level" value="SSC"> SSC</label></div>                                    
                                    <div><label><input type="radio" name="level" value="HSC"> HSC</label></div>
                                    <div><label><input type="radio" name="level" value="Honors"> Honors</label></div>                                   
                                    <div><label><input type="radio" name="level" value="PhD"> PhD</label></div>
                                    <div><label><input type="radio" name="level" value="Diploma"> Diploma</label></div>
                                    <div><label><input type="radio" name="level" value="Msc"> Msc</label></div>                                   
                                </div>
                                <button type="submit" class="btn btn-success glyphicon glyphicon-plus">Add</button>
                            </form>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>
            </div>


<!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>



            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
