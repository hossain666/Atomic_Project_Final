<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Book\BookTitle;

$book = new BookTitle();
$books = $book->index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-book"></span> 
                        Book
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="create.php">Add</a></li>
                        <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">Book Title</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>

        <?php
        if (isset($_GET['msg'])) {
            ?>
            <span class="alert alert-success"><?= $_GET['msg']; ?></span>
            <?php
        }
        ?>

        <div class="container">
            <a href="create.php"> <button type="button" class="btn btn-success pull-right" >Add new title</button></a>
        </div>
        <br>




        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">



                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>SL</th>                           
                            <th>Author</th>
                            <th>Title</th>
                            <th>Action</th>


                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $serial = 1;
                        foreach ($books as $book) {
                            ?>
                            <tr >
                                <td><?= $serial; ?></td>
                                <td><?= $book['author']; ?></td>
                                <td><?= $book['title']; ?></td>                               
                                <td>
                                    <a href="edit.php?id=<?php echo $book['id'];?>"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></a>
                                    <a href="show.php?id=<?php echo $book['id']; ?>"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></a>
                                    <a href="delete.php?id=<?php echo $book['id'];?>"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></a>
                                    <button type="button" class="btn btn-danger">Trash</button> 
                                    <button type="button" class="btn btn-success">Email</button> 
                                </td>  
                            </tr>
                            <?php
                            $serial++;
                        }
                        ?>
                    </tbody>   
                </table>
            </div> 
            <div class="col-md-2">
            </div>   
        </div>     


        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>






        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
