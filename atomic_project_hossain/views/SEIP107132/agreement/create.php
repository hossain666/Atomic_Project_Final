<!DOCTYPE html>
<html>
    <head>
        <title>TermsAndCon</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-education"></span> 
                        TermsAndCon
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="#"> View</a></li>
                        <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">Terms and Conditions</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>

        <div class="container">
            <div class="jumbotron">
                <!-- Example row of columns -->
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-8">

                        <form role="form" action="store.php" method="POST">
                            <div class="form-group">
                                <label for="title" >Title</label>
                                <input autofocus="autofocus"
                                  id="name"
                                  placeholder="Enter your name" 
                                  type="text" 
                                  name="title"
                                  required="required"
                                  size="30"
                                  class="form-control"
                                >
                                <label for="comment">Terms & Conditions</label>
                                <textarea class="form-control" rows="5" 
                                id="name"                              
                                type="text" 
                                name="term"
                                >
                                    
                                </textarea>
                            </div>
                            <button  type="submit" class="btn btn-primary">Save</button>
                            <button  type="submit" class="btn btn-primary">Save & Add Again</button>
                            <button  type="reset" value="reset" class="btn btn-danger">Reset</button>


                        </form>  
                    </div>


                </div>
                <div class="col-md-2">
                </div>
            </div>                    
        </div>

        <br>
        <br>
        <br>
        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Designed & Developed by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
