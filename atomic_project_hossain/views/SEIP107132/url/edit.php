<?php

    include_once '../../../vendor/autoload.php';
    use App\Bitm\SEIP107132\url\Bookmark;
    
    $bookmark = new Bookmark();
    $singlebook = $bookmark->show($_GET['id']);
?>


<!DOCTYPE html>
<!--
-->
<html>
    <head>
        <title>Bookmark</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
        
   
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-education"></span> 
            Bookmark
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="#"> View</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
                
     

                    
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">Bookmark Tools</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>


            
            <div class="container">
                <div class="jumbotron">

                    <form role="form" action="update.php" method="post">
                        
                        <input autofocus="autofocus"
                              id="id"
                              type="hidden" 
                              name="id"
                              value="<?php echo $singlebook ['id']?>"
                              
                         >
                        <div class="form-group">
                        
                        <label for="title" >Title</label>
                        <input autofocus="autofocus"
                              id="title"
                              placeholder="Enter the title here" 
                              type="text" 
                              name="title"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $singlebook ['title']?>"
                        >
                        </div>
                        
                        <label for="title" >URL</label>
                        <input autofocus="autofocus"
                              id="url"
                              placeholder="Enter the url here" 
                              type="text" 
                              name="url"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $singlebook ['url']?>"
                        > 
                        
                        <button  type="submit">Update</button>
                        <button  type="submit">Save & Add Again</button>
        <!--                <input type="submit" value="Save" />-->
                        <input type="reset" value="Reset" />
                    </form>
                    
                </div>
            </div>

      <!--footer --> 
             <div class="container">
                <div class="panel panel-default">
                    <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                    <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                    </div>
                </div>
             </div>

       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
