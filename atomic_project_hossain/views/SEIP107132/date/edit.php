<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Date\Birthday;

$birthobj = new Birthday();
$dates = $birthobj->show($_GET['id']);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon glyphicon-gift"></span> 
            Birthday
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="create.php"> Add</a></li>
                    
                    </ul>
                </div>
            </div>
           </nav>
          
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">
                      Edit Birthday
                      </h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>

            <div class="container">
                <div class="jumbotron">

                    <form role="form" action="update.php" method="POST">
                        <input type="hidden" name="id" <?php echo $dates ['id']?>>
                        <div class="form-group">
                        <label for="usr">Edit Buddy's Name:</label>
                        <input type="text" class="form-control" 
                        id="usr"
                        placeholder="Enter Buddy's Name"
                        type="text" 
                        name="name"
                        required="required"
                        size="30"
                        class="form-control"
                        value="<?php echo $dates ['name']?>"

                          >

                        <label for="title" >Date</label>
                        <input autofocus="autofocus"
                              id="date"
                              placeholder="Enter the title of your favorite book" 
                              type="text" 
                              name="date"
                              required="required"
                              size="30"
                              class="form-control"
                              value="<?php echo $dates ['date']?>"
                        >
                        </div>
                        <button  type="submit" class="btn btn-primary">Update</button>
   

                        </form>  
                </div>
            </div>

      
        
       
           
        
       
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
