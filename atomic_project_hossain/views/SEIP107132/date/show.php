<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Date\Birthday;

$birthobj = new Birthday();
$dates = $birthobj->show($_GET['id']);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-gift"></span> 
                        Birthday
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav ">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="create.php">Add</a></li>                  
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">List of Buddy's Name</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>       

        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">

                <div class="well">
                    <h1>
                    <lebel><b>Name:</b> </lebel><?php echo $dates ['name'];?> <br>
                    <lebel><b>Date: </b></lebel><?php echo $dates ['date'];?>
                    </h1>
                </div>
                <a href="index.php" class="btn btn-success"><span class="glyphicon glyphicon-home"></span>Back </a>
                

            </div> 
            <div class="col-md-2">
            </div>   
        </div>     

        <!-- pager -->

        <div class="container">
            <ul class="pager">
                <li><a href="#">Previous</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </div>


         <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
