<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP107132\Date\Birthday;
use App\Bitm\SEIP107132\Utility\Message;

$birthobj = new Birthday();
$dates = $birthobj->index();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">
            <span class="glyphicon glyphicon-gift"></span> 
            Birthday
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav ">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="create.php">Add</a></li>                    
                    </ul>
                </div>
            </div>
           </nav>
       
             <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">List of Buddy's Name</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
             </div>    
        
        <div class="container">
        <?php
        echo Message::message();
        ?>
        </div>
        
        <div class="container">
            <a href="create.php"> <button type="button" class="btn btn-success pull-right" >Add new entry</button></a>
        </div>
            
        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">
 
            <table class="table table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Buddy's Name</th>
                <th>Date</th>
                <th>Action</th>                            
            </tr>
            </thead>
            
            <tbody>
            <?php
            $serial=1;
            foreach($dates as $date){
            ?>
            <tr>
                <td><?php echo $serial;?></td>
                <td><?php echo $date['name'];?></td>
                <td><?php echo $date['date'];?></td>
                <td><a href="edit.php?id=<?php echo $date['id'];?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</a></td>
                <td><a href="show.php?id=<?php echo $date['id'];?>" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</a></td>
                <td>
                    <form action="delete.php" method="POST" >
                        <input type="hidden" name="id" value="<?php echo $date['id'];?>" >
                        <button type="submit" class="btn btn-success deleteBtn"><span class="glyphicon glyphicon-trash"></span>Delete</button>
                     </form>
                </td>
                <td><a href="trash.php?id=<?php echo $date['id'];?>" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>Trash</a></td>
                 <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            <?php
            $serial++;
            }
            ?>
            </tbody>   
            </table>
            </div> 
            <div class="col-md-2">
            </div>   
            </div>     
       
            <!-- pager -->
        
            <div class="container">
            <ul class="pager">
            <li><a href="#">Previous</a></li>
            <li><a href="#">Next</a></li>
            </ul>
            </div>
     
            <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>
 
         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../../resource/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
        
        <script>
            $('.alert').fadeOut(4000);
            
            $('.deleteBtn').bind('click', function(e){
                if(confirm("Do you want to delete this?") == false){
                    e.preventDefault();
                }
            });
        </script>
    </body>
</html>
