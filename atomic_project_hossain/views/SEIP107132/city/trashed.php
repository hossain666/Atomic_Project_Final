<?php
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP107132\city\SingleCity;
use App\Bitm\SEIP107132\Utility\Message;

$cityobj = new SingleCity();
$cities = $cityobj->trashed();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>City</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-education"></span> 
                        City
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="index.php">List View</a></li>
                        <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">

                <h1 class="text-center">City Selection</h1>
                <br>
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
            </div>
        </div>

        <?php
        echo Message::message();
        ?>


        <div class="container">
            <a href="create.php"> <button type="button" class="btn btn-success pull-right" >Add new level</button></a>
        </div>


        <div class="row">
            <div class="col-md-1">
            </div>    
            <div class="col-md-8">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>City </th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $serial = 1;
                        foreach($cities as $city){
                        ?>
                        <tr >
                            <td><?php echo $serial?></td>
                            <td><?php echo $city['name'];?></td>
                            <td><?php echo $city['city'];?> </td>
                            <td>
                            
<!--                            <a href="edit.php?id=<?php echo $city['id'];?>"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></a>-->
                           
                            <a href="show.php?id=<?php echo $city['id'];?>"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></a>                           
                            
                            <form action="delete.php" method="POST" style="display:inline">
                                <input type="hidden" name="id" value="<?php echo $city['id'];?>" >
                                <button type="submit" class="btn btn-success deleteBtn"><span class="glyphicon glyphicon-trash"></span>Delete</button>
                            </form>
<!--                            <a href="trash.php?id=<?php echo $city['id'];?>"> <button type="button" class="btn btn-danger">Trash</button></a>
                            <a href="trashed.php?id=<?php echo $city['id'];?>"> <button type="button" class="btn btn-danger">All trashed</button></a>-->
                            <a href="recover.php?id=<?php echo $city['id'];?>"><button type="submit" class="btn btn-success">Recover</button></a> 
                            </td>  
                        </tr>
                        <?php
                        $serial++;
                        }
                        ?>
                    </tbody>   
                </table>
            </div> 
            <div class="col-md-1">
            </div>   
        </div>     


        <!-- Pager -->
        <ul class="pager">
            <li><a href="#">Previous</a></li>

            <!--Pagination -->
            <ul class="pagination">
                <li><a href="#">1</a></li>
                <li class="active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>

            <li><a href="#">Next</a></li>
        </ul>

        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>






        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../../../resource/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
        
        <script>
            $('.alert').fadeOut(4000);
            
            $('.deleteBtn').bind('click', function(e){
                if(confirm("Do you want to delete this?") == false){
                    e.preventDefault();
                }
            });
        </script>
    </body>
</html>
