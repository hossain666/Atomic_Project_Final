<!DOCTYPE html>
<!--
-->
<html>
    <head>
        <title>Organization</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
        <link rel="../../../resource/stylesheet" href="style.css">
    </head>
    <body>
               
               
   
        
          <nav class="navbar navbar-inverse">
            <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="#">
            <span class="glyphicon icon-birthday-cake"></span> 
            Organization
            </a>
            </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="create.php">Add</a></li>
                    <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
           </nav>
                
     

                    
            <div class="jumbotron">
                 <div class="container">

                  <h1 class="text-center">List of Organizations</h1>
                  <br>
                  <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                 </div>
            </div>


            
        <div class="row">
            <div class="col-md-2">
            </div>    
            <div class="col-md-8">
                
            
           
            <table class="table table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Organization's Name</th>
                <th>Summary</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Trash</th>
                <th>Email</th>
                
            </tr>
            </thead>
            
            <tbody>
            <tr >
                <td>01</td>
                <td>International Court of Justice </td>
                <td>Lorem Ipsum</td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            <tr >
                <td>02</td>
                <td>International Labour Organization </td>
                <td>Lorem Ipsum </td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            <tr >
                <td>03</td>
                <td>International Health Organization </td>
                <td>Lorem Ipsum </td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            <tr >
                <td>04</td>
                <td>BASIS Institute of Management </td>
                <td>Lorem Ipsum</td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            
            <tr >
                <td>05</td>
                <td>Dhaka University IT Society </td>
                <td>Lorem Ipsum </td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            
            <tr >
                <td>06</td>
                <td>Hossain Software Firm</td>
                <td>Lorem Ipsum  </td>
                <td><a href="edit.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Edit</button></td></a>
                <td><a href="single_view.php"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-sunglasses"></span>view</button></td></a>
                <td><a href="#"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-trash"></span>Delete</button></td></a>
                <td><button type="button" class="btn btn-danger">Trash</button> </td>  
                <td><button type="button" class="btn btn-success">Email</button> </td>  
            </tr>
            </tbody>   
            </table>
            </div> 
            <div class="col-md-2">
            </div>   
            </div>     
            
        <!--footer --> 
             <div class="container">
                <div class="panel panel-default">
                    <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                    <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                    </div>
                </div>
             </div>

           
        
       
         
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
