<?php

namespace App\Bitm\SEIP107132\Utility;

class Direction {
    
    static public function redirect($url = "index.php"){
        header("Location: ".$url);
    }
}
