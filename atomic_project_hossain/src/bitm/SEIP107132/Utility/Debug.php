<?php

namespace App\Bitm\SEIP107132\Utility;

class Debug {
        
    static public function d($val = FALSE){
        echo '<pre>';
        var_dump($val);
        echo '</pre>';
    }
    
    static public function dd($val = FALSE){
        self::d($val);
        die();
    }
}
