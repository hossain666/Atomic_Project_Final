<?php

namespace App\Bitm\SEIP107132\Utility;
session_start();

class Message {
    
    static public function message($message = FALSE){
        if($message){
            self::setMessage($message);
        } else{
            $_message = self::getMessage();
            return $_message;
        }
    }

    static private function getMessage(){
        $msg = $_SESSION['message'];
        $_SESSION['message'] = NULL;
        return $msg;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}
