 <!DOCTYPE html>
<!--
-->
<html>
    <head>
        <title>Main Project</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="resource/stylesheet" href="style.css">
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
     </head>
    <body>




        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <span class="glyphicon glyphicon-leaf"></span> 
                        Atomic Project
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                         <li><a href="#">View</a></li> 
                    </ul>
                </div>
            </div>
        </nav>




        <div class="jumbotron">
            <div class="container">

                <img src="resource/images/atomic.jpg">
                <p class="text-center"> <code>Ahmed Al Hossain</code> </p>
                <p class="text-center"> <code>SEIP-107132</code> </p>
                <p class="text-center"> <code>Batch 10 th</code> </p>
            </div>
        </div>

        <div class="container">
            <a href="create.php"> <button type="button" class="btn btn-success pull-right" >Add new project</button></a>
        </div>
        <br>

        <div class="row">
            <div class="col-md-3">
            </div>    
            <div class="col-md-6"> 

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Project Name</th>
                            <th>Overview</th>
                            <th>Action</th>
                             
                        </tr>
                    </thead>

                    <tbody>
                        <tr >
                            <td>01</td>
                            <td>Book Title</td>
                            <td>
                                
                                
                                

   <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Overview</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
   // <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
                                
                                
                            </td>
                            <td>
                                <a href="edit.php"><button type="button" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        Details</button>
                            </td>
                           
                             
                        </tr>
                         
                    </tbody>   
                </table>
           
           <div class="col-md-3"> </div> 
      


         
        

        <!--footer --> 
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-footer">
                    <p class="text-center">Design & Develop by
                        <a href="https://www.facebook.com/ahmed.a.hossain" target="_blank">@Hossain</a></p>
                </div>
            </div>
        </div>






        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="resource/js/bootstrap.min.js"></script>
    </body>
</html>
